## 4.1.1 (2023-10-13)

### Changed

- 基于 @vitejs/plugin-legacy@4.1.1 减配而来

## 0.2.0 (2023-02-03)

### Changed

- 基于 @vitejs/plugin-legacy@4.0.1 减配而来

## 0.1.1 (2022-12-23)

### Changed

- 基于 @vitejs/plugin-legacy@3.0.1 减配而来

## 0.0.4 (2022-11-01)

### Fixed

- 修正补充导出对象异常

## 0.0.3 (2022-10-31)

### Changed

- 基于 @vitejs/plugin-legacy@2.3.0 减配而来
- 更新所有非主要依赖项
- system 打包输出在打包根目录，避免路径匹配 Bug

## 0.0.1 (2022-10-20)

### Added

- 基于 @vitejs/plugin-legacy@2.2.0 减配而来
